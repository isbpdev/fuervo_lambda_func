'use strict';
const AWS = require('aws-sdk');
const axios = require('axios');

const kmsEncryptedHookUrl = process.env.ENCRYPTED_URL;
const slackChannel = process.env.SLACK_CHANNEL;
let decryptedUrl;

const processEvent = async (url, event) => {
    const text = event.Records[0].Sns.Message + "\n-----------------------------";
    const data = {
        channel: slackChannel,
        text: `${text}`
    };

    const requestConfig = {
        headers: {
            "Content-Type": "application/json",
            'Content-Length': Buffer.byteLength(JSON.stringify(data))
        },
        timeout: 5000
    };

    const response = await axios.post(url, data, requestConfig);

    if (response.status < 400) {
        return 'process end.';
    } else if (response.status < 500) {
        // 4xx error. Don't retry because the error is due to a problem with the request
        // TODO: notify another way
        const message = `Error posting message to Slack API: ${response.status} - ${response.statusText}`;
        console.error(message);
        console.error(response.data);
        throw new Error(message);
    } else {
        // 5xx error. Let Lambda retry
        // TODO: SQS
        const message = `Server error when processing message: ${response.status} - ${response.statusText}`;
        console.error(message);
        console.error(response.data);
        throw new Error(message);
    }
};

const decryptText = async (encryptedText) => {
    const kms = new AWS.KMS();
    const params = { CiphertextBlob: new Buffer(encryptedText, 'base64') };
    const data = await kms.decrypt(params).promise();
    return `https://${data.Plaintext.toString('ascii')}`;
};

exports.handler = async (event, context) => {
    // Container reuse, simply process the event with the hookUrl in memory
    if (!decryptedUrl) {
        if (kmsEncryptedHookUrl) {
            decryptedUrl = await decryptText(kmsEncryptedHookUrl);
        } else {
            throw new Error('Hook URL has not been set.');
        }
    }

    return await processEvent(decryptedUrl, event);
};