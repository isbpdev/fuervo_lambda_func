var AWS = require('aws-sdk');
var gm = require('gm').subClass({ imageMagick: true });
var s3 = new AWS.S3();

exports.handler = function(event, context, callback) {

  var bucket = event.requestContext.stage==='local' ? "local-image.fuervo.jp" : "stg-image.fuervo.jp"; // 変換元画像のバケット名
  var key = decodeURIComponent(event.pathParameters.proxy);     // 変換元画像のキー名
  console.log(bucket);
  
  const done = (err, body, acceptRanges, contentType, etag, lastModified) => callback(null, {
        statusCode: err ? '400' : '200',
        body: err ? (err.message || err) : body,
        headers: {
            'AcceptRanges': acceptRanges,
            'last-modified': lastModified,
            'Content-Type': contentType,
            'etag': etag,
            'Cache-Control': 'max-age=2592000, s-maxage=259200'
        },
        isBase64Encoded: true,
    });

  // 拡張子を抽出
  var typeMatch = key.match(/\.([^.]*)$/);
  if (!typeMatch) {
    done("Could not determine the image type.");
    return;
  }

  // 拡張子がjpg, jpeg, pngのものだけ許可する
  var imageType = typeMatch[1];
  if (imageType != "jpg" && imageType != "jpeg" && imageType != "png") {
    done('Unsupported image type: ${imageType}');
    return;
  }

  // S3の画像にアクセス
  s3.getObject({Bucket: bucket, Key: key}, function(err, data){
    if (err) { context.done('error getting object', err); }
// console.log(data);
    // 画像処理
    gm(data.Body)
    .resize(null, '168')
    .borderColor('#f3f5f7')
    .border('245', '245')
    .gravity('Center')
    .crop('233', '168')
    .stream(function(err,stdout,stderr){ // ストリームで出力する
      if(err){
        console.log("gm process error");
        console.log(err,stdout,stderr);
        context.fail(err);
      }
      var chunks = []; // ストリームのチャンクを溜め込むための配列
      stdout.on('data',function(chunk){
        console.log('pushed');
        chunks.push(chunk); // チャンクを溜め込む
      });
      stdout.on('end',function(){
        console.log('end');
        var buffer = Buffer.concat(chunks); // 最後まで溜め込んだら結合してバッファに書き出す
        // done(null, buffer.toString('base64'));
        done(null, buffer.toString('base64'), data.AcceptRanges, data.ContentType, data.ETag, data.LastModified);
      });

      stderr.on('data',function(data){
        console.log('stderr data: ' +  data);
      });
    });
  });
};