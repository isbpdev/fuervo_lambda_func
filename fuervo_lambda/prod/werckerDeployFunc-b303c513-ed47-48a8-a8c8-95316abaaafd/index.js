'use strict';
const AWS = require('aws-sdk');
const qs = require('querystring');
const https = require('https');

/** 暗号化されたslackトークン */
const kmsSlackToken = process.env.kmsSlackToken;
/** 内部メモリslackトークン */
let token;

/** werckerAPIリクエスト用オプション */
const options = {
    hostname: 'app.wercker.com',
    path: '/api/v3/runs',
    headers: {
        // 'Authorization': 'Bearer ' + process.env.kmsWerckerToken, 追々、環境変数を復号化して使うようにする
        'Authorization': 'Bearer 3925e4e3cf3ebced76d2f682a5fa87967678f806e5723df032d85aaba5adbce0',
        'Content-Type': 'application/json'
    },
    method: 'POST'
};

/** werckerのパイプラインID */
const pipelineMap = new Map([
    ["prod-admin", "5a290176888b6a0100e0d3cd"],
    ["prod-owner", "5a29019a6f2c70010077c450"],
    ["prod-player", "5a2901ae7dd43901000a01e9"],
    ["stg-admin", "5a29015e229b49010031bfa2"],
    ["stg-owner", "5a2901897dd43901000a01e6"],
    ["stg-player", "5a2901c19b22360100b10909"]]);

/**
 * 
 * 実処理
 * 
 **/
function processEvent(event, callback) {
    const params = qs.parse(event.body);
    const requestToken = params.token;
    if (requestToken !== token) {
        console.error(`Request token (${requestToken}) does not match expected`);
        return callback('Invalid request token');
    }

    var req = https.request(options, (res) => {
        let body = '';
        console.log('Status:', res.statusCode);
        console.log('Headers:', JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', (chunk) => body += chunk);
        res.on('end', () => {
            // If we know it's JSON, parse it
            if (res.headers['content-type'] === 'application/json') {
                body = JSON.parse(body);
            }
            callback(null, body);
            console.log('Successfully processed HTTPS response');
        });
    });
    
    req.on('error', (err) => {
        console.log('Error:', err.message);
        callback(null, `Error: ${err.message}`);
    });
    
    const commandText = params.text;
    const commandArgs = commandText.split(/\s+/, 4);
    const envService = `${commandArgs[1]}-${commandArgs[2]}`;
    var deployMsg = commandArgs[3] ? commandArgs[3] : `deploy from slack by ${params.user_name} in ${params.channel_name} with the following text: ${commandText}`;
    var data = {
        pipelineId: pipelineMap.get(envService),
        message: deployMsg,
        sourceRunId: commandArgs[0]
    };
    req.write(JSON.stringify(data));
    req.end();
}

/**
 * Pass the data to send as `event.data`, and the request options as
 * `event.options`. For more information see the HTTPS module documentation
 * at https://nodejs.org/api/https.html.
 *
 * Will succeed with the response body.
 */
exports.handler = (event, context, callback) => {
    console.log(JSON.stringify(event.body, null, '  '));
    
    const done = (err, res) => callback(null, {
        statusCode: err ? '400' : '200',
        body: err ? (err.message || err) : JSON.stringify(res,undefined,2),
        headers: {
            'Content-Type': 'application/json',
        },
    });
    
    if (token) {
        // Container reuse, simply process the event with the key in memory
        processEvent(event, done);
    } else if (kmsSlackToken) {
        const cipherText = { CiphertextBlob: new Buffer(kmsSlackToken, 'base64') };
        const kms = new AWS.KMS();
        kms.decrypt(cipherText, (err, data) => {
            if (err) {
                console.log('Decrypt error:', err);
                return done(err);
            }
            token = data.Plaintext.toString('ascii');
            processEvent(event, done);
        });
    } else {
        done('Token has not been set.');
    }
};
