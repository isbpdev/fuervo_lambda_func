const AWS = require('aws-sdk');
const SQS = new AWS.SQS({apiVersion: '2012-11-05'});
const SNS = new AWS.SNS({apiVersion: '2010-03-31'});
const QUEUE_URL = process.env.QUEUE_URL;
const MAX_NUMBER_OF_MESSAGES = process.env.MAX_NUMBER_OF_MESSAGES;
const SNS_ARN = process.env.SNS_ARN;

const recieveMessages = () => {
    const params = {
        QueueUrl: QUEUE_URL,
        MaxNumberOfMessages: MAX_NUMBER_OF_MESSAGES, //指定した数が取れるとは限らないらしい
        VisibilityTimeout: 30
    };

    return SQS.receiveMessage(params).promise();
};

const publishMessage = (data) => {
    return Promise.all(data.Messages.map((message) => {
        console.log(message);
        // const params = {
        //     Message: `${message}`,
        //     TopicArn: `${SNS_ARN}`
        // };
        // return sns.publish(params).promise();
        return Promise.resolve(message);
    }));
};

exports.handler = (event, context, callback) => {
    recieveMessages()
        .then(publishMessage)
        .then((result) => callback(null, result))
        .catch(callback);
};