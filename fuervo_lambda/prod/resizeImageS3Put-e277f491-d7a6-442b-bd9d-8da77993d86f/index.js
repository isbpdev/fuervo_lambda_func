'use strict';

const AWS = require('aws-sdk');
const Sharp = require('sharp');
const s3 = new AWS.S3();

const BUCKET = process.env.BUCKET;
const URL = process.env.URL;
const ALLOWED_DIMENSIONS = new Set();
const S3_KEY_PREFFIX = process.env.S3_KEY_PREFFIX;

if (process.env.ALLOWED_DIMENSIONS) {
  const dimensions = process.env.ALLOWED_DIMENSIONS.split(/\s*,\s*/);
  dimensions.forEach((dimension) => ALLOWED_DIMENSIONS.add(dimension));
}

exports.handler = function(event, context, callback) {
console.log(event);
  // リクエストパス
  const key = decodeURIComponent(event.pathParameters.proxy);
  // リクエスト画像サイズ
  const match = key.match(/((\d+)x(\d+))\/(.*)/);
  const dimensions = match[1];
  const width = parseInt(match[2], 10);
  const height = parseInt(match[3], 10);
  // 変換元画像のキー名
  const originalKey = match[4];
  
  // callback
  const done = (err, msg) => callback(null, {
        statusCode: err ? '403' : '301',
        body: err ? err : msg,
        headers: err ? {} : {'location': `${URL}${event.pathParameters.proxy}`},
    });

    // 許可したサイズか
    if(ALLOWED_DIMENSIONS.size > 0 && !ALLOWED_DIMENSIONS.has(dimensions)) {
      done('disallowed dimention.');
      return;
    }

  // 拡張子を抽出
  var typeMatch = key.match(/\.([^.]*)$/);
  if (!typeMatch) {
    done('can\'t get ext.');
    return;
  }

  // 拡張子がjpg, jpeg, pngのものだけ許可する
  var ext = typeMatch[1].toLowerCase();
  if (ext != "jpg" && ext != "jpeg" && ext != "png") {
    done('disallowed ext.');
    return;
  }
  var contentType = ext === 'png' ? 'png' : 'jpeg' ;

  s3.getObject({Bucket: `${BUCKET}`, Key: originalKey}).promise()
  .then(data =>
    Sharp(data.Body)
      .resize(width, height)
      .toFormat(contentType)
      .toBuffer()
  )
  .then(image => s3.putObject({
      Body: image,
      Bucket: `${BUCKET}`,
      ContentType: `image/${contentType}`,
      CacheControl: 'max-age=2592000, s-maxage=259200',
      Key: `${S3_KEY_PREFFIX}${key}`,
    }).promise()
  )
  .then(() => done(null, `${URL}${key}`)
  )
  .catch(err => done(err));
};
