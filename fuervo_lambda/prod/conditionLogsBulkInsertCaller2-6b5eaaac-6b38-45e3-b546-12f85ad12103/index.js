const AWS = require('aws-sdk');
const sns = new AWS.SNS();
const s3 = new AWS.S3({apiVersion: '2006-03-01'});

const CALLEE_ARN = process.env.CALLEE_ARN;
const S3_BUCKET = process.env.S3_BUCKET;
const CONDITION_LOGS_PREFIX = process.env.CONDITION_LOGS_PREFIX;

const getLogs = (params, out=[]) => {
    return new Promise((resolve, reject) => {
        s3.listObjectsV2(params).promise()
            .then(({Contents, IsTruncated, NextContinuationToken}) => {
                out.push(...Contents);
                !IsTruncated ? resolve(out) : resolve(getLogs(Object.assign(params, {ContinuationToken: NextContinuationToken}), out));
            })
            .catch(reject);
    });
};

const publishSns = (keys) => {
    return Promise.all(keys.map(({Key}) => {
        const s3PutEventMessage = {
            "Records": [
              {
                "s3": {
                  "object": {
                    "key": `${Key}`
                  }
                }
              }
            ]
        };
        console.log(JSON.stringify(s3PutEventMessage));
        const params = {
            Message: `${JSON.stringify(s3PutEventMessage)}`,
            TopicArn: `${process.env.SNS_ARN}`
        };
        // sns.publish(params, (err, data) => {
        //     if(err) console.log(err);
        // });

        // const params = {
        //     FunctionName: CALLEE_ARN,
        //     InvocationType: "Event",
        //     Payload: JSON.stringify(s3PutEventMessage),
        //     // Qualifier: "$LATEST"
        // };

        return sns.publish(params).promise();
    }));
};

exports.handler = (event, context, callback) => {
    const params = {
        Bucket: S3_BUCKET,
        Prefix: `${event.Prefix}${CONDITION_LOGS_PREFIX}`,
        StartAfter: `${event.Prefix}${CONDITION_LOGS_PREFIX}`,
    };

    getLogs(params)
        .then(publishSns)
        .then(data => callback(null, data))
        .catch(callback);
};