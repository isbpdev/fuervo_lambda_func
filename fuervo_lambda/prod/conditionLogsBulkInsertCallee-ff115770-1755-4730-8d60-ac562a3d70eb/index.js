const AWS = require('aws-sdk');
const LineStream = require('byline').LineStream;
const zlib = require('zlib');
const toArray = require('stream-to-array');
const S3_BUCKET = process.env.S3_BUCKET;
const s3 = new AWS.S3();
const sns = new AWS.SNS();
const crypto = require('crypto');
let s3Key;

const getConditionLogs = (key) => {
    const lineStream = new LineStream();
    const s3Stream = s3.getObject({Bucket: S3_BUCKET, Key: key})
        .createReadStream()
            .pipe(zlib.createGunzip())
            .pipe(lineStream);
    return toArray(s3Stream);
};

const arrayToDateString = (dateArray) => {
    if(dateArray) {
        if(dateArray.length == 3) {
            dateArray[1] = ('0'+dateArray[1]).slice(-2);
            dateArray[2] = ('0'+dateArray[2]).slice(-2);
            return dateArray.join('-');
        }
    }
    
    return null;
};

const arrayToTimeString = (timeArray) => {
    if(timeArray) {
        if(timeArray.length == 2) {
            timeArray[0] = ('0'+timeArray[0]).slice(-2);
            timeArray[1] = ('0'+timeArray[1]).slice(-2);
            return `${timeArray.join(':')}:00+09:00`;
        }
    }
    
    return null;
};

const getHash = (plain) => {
    const hash = crypto.createHash('sha256');
    hash.update(plain);
    return hash.digest('base64');
};

const getSignedRequest = (doc) => {
    const creds = new AWS.EnvironmentCredentials('AWS');
    const endpoint =  new AWS.Endpoint(process.env.ES_ENDPOINT);
    const req = new AWS.HttpRequest(endpoint, 'ap-northeast-1');
    const logDate = getLogDate(s3Key);
    req.method = 'POST';
    req.path = `/actiontest-${logDate}/condition/_bulk`;
    req.body = doc.join('\n')+'\n';
    console.log(doc.join('\n'));
    req.headers['presigned-expires'] = false;
    req.headers['Host'] = endpoint.host;
    req.headers['Content-Type'] = 'application/json';

    // Sign the request (Sigv4)
    const signer = new AWS.Signers.V4(req, 'es');
    signer.addAuthorization(creds, new Date());
    
    return Promise.resolve(req);
};

const postToES = (signerReq) => {
    return new Promise((resolve, reject) => {
        const send = new AWS.NodeHttpClient();
        send.handleRequest(signerReq, null,
            (httpResp) => {
                var body = '';
                httpResp.on('data', (chunk) => {
                    body += chunk;
                });
                httpResp.on('end', (chunk) => {
                    if(httpResp.statusCode < 400) {
                        resolve(body.toString());
                    } else {
                        console.log(`Error posting records to ES: ${httpResp.statusCode} - ${httpResp.statusMessage}`);
                        reject(new Error(body.toString()));
                    }
                });
            },
            (err) => {
                reject(err);
            }
        );
    });
};

const snsPublishIfErrorExists = (body) => {
    const bodyObj = JSON.parse(body);
    if(bodyObj.errors===true) {
        const errorItems = bodyObj.items.filter(item => item.index.status>=400);
        
        Promise.all(errorItems.map(item => {
            if(item.index.error) {
                return Promise.resolve(JSON.stringify(item.index.error, undefined, 2));
            }
        }))
        .then((errorMessage) => {
            const retryContents = {
                "Records": [{
                    "s3": {
                        "object": {
                            "key": `${s3Key}`
                        }
                    }
                }],
                "myMessage" : `${errorMessage}`
            };
            const params = {
                Message: `${JSON.stringify(retryContents)}`,
                TopicArn: `${process.env.SNS_ARN}`
            };
            sns.publish(params, (err, data) => {
                if(err) console.log(err);
            });
        });

    }
    return Promise.resolve(`process end.`);
};

const isConditionLog = (key) => {
    return key.includes(`fuervo-player_conditionLog`);
};

const searchFormDeleter = (key, value) => {
    if(key === 'trainSearchForm' || key === 'areaSearchForm') {
        return undefined;
    }
    return value;
};

const modifyTrainSearchForm = (conditionObj) => {
    // Deep Clone
    const modifiedJson  = JSON.parse(JSON.stringify(conditionObj, searchFormDeleter));
    modifiedJson.searchForm = {};
    modifiedJson.searchForm.commonForm = conditionObj.trainSearchForm.commonForm;
    modifiedJson.searchForm.trainForm = conditionObj.trainSearchForm;
    modifiedJson.searchForm.trainForm.commonForm = {};

    return modifiedJson;
};

const modifyAreaSearchForm = (conditionObj) => {
    // Deep Clone
    const modifiedJson  = JSON.parse(JSON.stringify(conditionObj, searchFormDeleter));
    modifiedJson.searchForm = {};
    modifiedJson.searchForm.commonForm = conditionObj.areaSearchForm.commonForm;
    modifiedJson.searchForm.areaForm = conditionObj.areaSearchForm;
    modifiedJson.searchForm.areaForm.commonForm = {};

    return modifiedJson;
};

const modifySearchForm = (conditionObj) => {
    // 開始・終了日が配列になっているので文字列になおす
    conditionObj.searchForm.startDate = arrayToDateString(conditionObj.searchForm.startDate);
    conditionObj.searchForm.endDate = arrayToDateString(conditionObj.searchForm.endDate);
    
    // 開始・終了時間が配列になっているので文字列になおす
    conditionObj.searchForm.startTime = arrayToTimeString(conditionObj.searchForm.startTime);
    conditionObj.searchForm.endTime = arrayToTimeString(conditionObj.searchForm.endTime);

    return conditionObj;
};

const mopdifyConditionLogs = (srcLogs) => {
    return Promise.all(srcLogs.map(srcLog => {
        const requestPathTemp = JSON.parse(srcLog.toString()).RequestPath;
        let conditionObj = JSON.parse(
                                srcLog.toString()
                                    .replace(/(eventSearchForm|facilitySearchForm)/g, 'searchForm')
                                    .replace(/cmnForm/g, 'commonForm'));
        conditionObj.RequestPath = requestPathTemp;
        
        if(conditionObj.searchForm) {
            conditionObj = modifySearchForm(conditionObj);
        } else if(conditionObj.areaSearchForm) {
            conditionObj = modifyAreaSearchForm(conditionObj);
        } else if(conditionObj.trainSearchForm) {
            conditionObj = modifyTrainSearchForm(conditionObj);
        } else if(/(year|month|day)/.test(conditionObj.RequestPath)) {
            // 一旦そのまま
            // おいおい必要があればsearchFormの型にあわせる
        } else {
            console.log(JSON.stringify(conditionObj));
            return Promise.reject(new Error(`invalid condition log.`));
        }
        
        // 冪等性を考慮してユーザーIDと検索日時から作成したハッシュ値をIDとする
        var hash = getHash(`${conditionObj.CreateDatetime}${conditionObj.CommonId}`);
        let doc = JSON.stringify({ index: {_id: hash} });
        doc += `\n${JSON.stringify(conditionObj)}`;
        return Promise.resolve(doc);
    }));
};

const getLogDate = (key) => {
    const splitedKey = key.split('.');
    //keyはこんな感じ。log[UNIXTIME]のところを取得すす
    // path/to/hoge_fuervo-player_conditionLog.YYYYMMDD.log1234567890.gz
    const unixtime = splitedKey[splitedKey.length-2].replace(/log/, "");
    return unixtime2ym(unixtime);
};

const unixtime2ym = (unixtime) => {
  const d = new Date( unixtime * 1000 ); // convert msec.
  const year  = d.getFullYear();
  const month = d.getMonth() + 1;
  const paddingMonth = ('0' + month).slice(-2);

  return `${year}${paddingMonth}`;
};

exports.handler = function(event, context, callback) {
    const message = JSON.parse(event.Records[0].Sns.Message);
    s3Key = message.Records[0].s3.object.key;
    if(!isConditionLog(s3Key)) {
        callback(null, `not target key: ${s3Key}`);
    } else {
        console.log(`Process start for ${s3Key}.`);
    }

    getConditionLogs(s3Key)
        .then(mopdifyConditionLogs)
        .then(getSignedRequest)
        .then(postToES)
        .then(snsPublishIfErrorExists)
        .then((result) => callback(null, result))
        .catch(callback);
};