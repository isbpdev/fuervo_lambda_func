const AWS = require('aws-sdk');
const lambda = new AWS.Lambda({apiVersion: '2015-03-31'});
const s3 = new AWS.S3({apiVersion: '2006-03-01'});

const CALLER2_ARN = process.env.CALLER2_ARN;
const EB_ENVIRONMENT_ID = process.env.EB_ENVIRONMENT_ID;
const S3_BUCKET = process.env.S3_BUCKET;
const S3_PREFIX = process.env.S3_PREFIX;

const getInstances = () => {
    const params = {
        Bucket: S3_BUCKET,
        // ContinuationToken: 'STRING_VALUE', 1000件を超えることはないであろう
        Delimiter: '/',
        Prefix: `${S3_PREFIX}/${EB_ENVIRONMENT_ID}/`,
      };
      return s3.listObjectsV2(params).promise();
};

const invokeCaller2 = (instances) => {
    return Promise.all(instances.CommonPrefixes.map((instance) => {
        const params = {
            FunctionName: CALLER2_ARN,
            InvocationType: "Event",
            Payload: JSON.stringify(instance),
            // Qualifier: "$LATEST"
        };
        return lambda.invoke(params).promise();
    }));
};

exports.handler = (event, context, callback) => {
    getInstances()
      .then(invokeCaller2)
      .then(data => callback(null, data))
      .catch(callback);
};