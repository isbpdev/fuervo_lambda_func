const https = require('https');
const slack_req_opts = {
    hostname: 'hooks.slack.com',
    path: '/services/T02TMDTEY/B7N4FH95Z/pdX087u1yx5cHuSVGYj0iV7o',
    headers: {
        'Content-Type': 'application/json'
    },
    method: 'POST'
};

const RequestToSlack = (text) => {
    return new Promise((resolve, reject) => {
        const req = https.request(slack_req_opts, res => {
            let body = '';
            res.setEncoding('utf8');
            res.on('data', chunk => body += chunk);
            res.on('end', () => {
                resolve(body);
            });
        });

        req.on('error', err => {
            reject(err);
        });

        const payload = {
            channel: '#kunipon_channel',
            username: 'kunipon-lambda',
            icon_emoji: ':kunipon:',
            text: `${text}`
        };
        req.write(JSON.stringify(payload, null, '  '));
        req.end();
    });
};

const getText = event => {
    let text = 'test';
    if(event.Records) {
        if (event.Records[0].Sns) {
            text = `${event.Records[0].Sns.TopicArn} - ${event.Records[0].Sns.Message}`;
        }
    }

    return Promise.resolve(text);
};

exports.handler = (event, context, callback) => {
    console.log(event);

    const done = (err, res) => callback(null, {
        statusCode: err ? '400' : '200',
        body: err ? (err.message || err) : JSON.stringify(res),
        headers: {
            'Content-Type': 'application/json',
        }
    });

    getText(event)
        .then(RequestToSlack)
        .then(result => done(null, result))
        .catch(done);
};